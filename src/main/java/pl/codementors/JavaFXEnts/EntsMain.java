package pl.codementors.JavaFXEnts;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.ResourceBundle;

public class EntsMain extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        ResourceBundle rb = ResourceBundle.getBundle("view.messages.ents_list_msg");
        FXMLLoader loader = new FXMLLoader(EntsMain.class.getResource("/view/ents_list.fxml"), rb);
        Parent root = loader.load();
        Scene scene = new Scene(root);
        scene.getStylesheets().add("/css/ents_list.css");
        stage.setScene(scene);
        stage.show();
    }
}

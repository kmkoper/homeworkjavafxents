package pl.codementors.JavaFXEnts.Workers;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.concurrent.Task;
import pl.codementors.JavaFXEnts.models.Ent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OpenWorker extends Task<Void> {

    private static final Logger log = Logger.getLogger(SaveWorker.class.getCanonicalName());

    private Collection<Ent> entsList;
    private File file;

    public OpenWorker(Collection<Ent> entsList, File file) {
        this.entsList = entsList;
        this.file = file;
    }

    @Override
    protected Void call() {
        entsList.clear();
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            int size = (Integer) ois.readObject();
            for (int i = 0; i < size; i++) {
                Ent ent = (Ent) ois.readObject();
                Platform.runLater(() -> entsList.add(ent));
                Thread.sleep(100);
                updateProgress(i + 1, size);
            }
            Thread.sleep(1000);
            updateProgress(0, size);
        } catch (IOException | ClassNotFoundException | InterruptedException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return null;
    }
}

package pl.codementors.JavaFXEnts.Workers;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.stage.FileChooser;
import pl.codementors.JavaFXEnts.models.Ent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SaveWorker extends Task {
    private static final Logger log = Logger.getLogger(SaveWorker.class.getCanonicalName());

    private List<Ent> entsList;
    private File file;

    private DoubleProperty progress = new SimpleDoubleProperty(0);

    public SaveWorker(ObservableList<Ent> entsList, File file) {
        this.entsList = entsList;
        this.file = file;
    }

    @Override
    protected Object call() {

        try {
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            oos.writeObject(entsList.size());
            entsList.stream().sequential().forEach(ent -> {
                try {
                    oos.writeObject(ent);
                    Thread.sleep(200);
                    increaseProgress(entsList.indexOf(ent) + 1);
                } catch (IOException | InterruptedException ex) {
                    log.log(Level.WARNING, ex.getMessage(), ex);
                }
            });
            Thread.sleep(2000);
            resetProgress();
        } catch (IOException | InterruptedException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return null;
    }

    private void increaseProgress(int currentIndex) {
        updateProgress(currentIndex, entsList.size());
    }
    private void resetProgress(){
        updateProgress(0, entsList.size());
    }

}

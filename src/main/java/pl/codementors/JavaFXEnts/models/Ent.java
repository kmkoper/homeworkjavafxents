package pl.codementors.JavaFXEnts.models;

import java.io.Serializable;

public class Ent implements Serializable {
    private int yearOfPlanting;
    private int height;
    private String species;

    public enum Type {
        LEAFED,
        CONIFEROUS,
        UNDEFINED;
    }

    private Type type;


    public Ent(int yearOfPlanting, int height, String species, Type type) {
        this.yearOfPlanting = yearOfPlanting;
        this.height = height;
        this.species = species;
        this.type = type;
    }

    public int getYearOfPlanting() {
        return yearOfPlanting;
    }

    public Ent setYearOfPlanting(int yearOfPlanting) {
        this.yearOfPlanting = yearOfPlanting;
        return this;
    }

    public int getHeight() {
        return height;
    }

    public Ent setHeight(int height) {
        this.height = height;
        return this;
    }

    public String getSpecies() {
        return species;
    }

    public Ent setSpecies(String species) {
        this.species = species;
        return this;
    }

    public Type getType() {
        return type;
    }

    public Ent setType(Type type) {
        this.type = type;
        return this;
    }

    @Override
    public String toString() {
        return "Ent{" +
                "yearOfPlanting=" + yearOfPlanting +
                ", height=" + height +
                ", species='" + species + '\'' +
                ", type=" + type +
                '}';
    }
}

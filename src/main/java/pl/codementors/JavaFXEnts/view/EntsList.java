package pl.codementors.JavaFXEnts.view;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;
import pl.codementors.JavaFXEnts.Workers.OpenWorker;
import pl.codementors.JavaFXEnts.Workers.SaveWorker;
import pl.codementors.JavaFXEnts.models.Ent;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class EntsList implements Initializable {

    @FXML
    private TableView<Ent> entsView;

    @FXML
    private Button plant;

    @FXML
    private Button cut;

    @FXML
    private Button rootSeeding;

    @FXML
    private ProgressBar progressBar;

    @FXML
    private MenuBar menuBar;

    @FXML
    private Menu file;

    @FXML
    private Menu help;

    @FXML
    private MenuItem itemOpen;

    @FXML
    private MenuItem itemSave;

    @FXML
    private MenuItem itemAbout;

    @FXML
    private TableColumn<Ent, Integer> yearOfPlantingColumn;

    @FXML
    private TableColumn<Ent, Integer> heightColumn;

    @FXML
    private TableColumn<Ent, String> speciesColumn;

    @FXML
    private TableColumn<Ent, Ent.Type> typeColumn;

    private ObservableList<Ent> entsList = FXCollections.observableArrayList();

//    private ObjectProperty<Ent> selectedEnt = new SimpleObjectProperty<>(null);

    private ResourceBundle rb;


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        rb = resourceBundle;

//        entsList.addAll(prepare());
        entsView.setItems(entsList);


        yearOfPlantingColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        yearOfPlantingColumn.setOnEditCommit(event -> event.getRowValue().setYearOfPlanting(event.getNewValue()));

        heightColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        heightColumn.setOnEditCommit(event -> event.getRowValue().setHeight(event.getNewValue()));

        typeColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(Ent.Type.values()));
        typeColumn.setOnEditCommit(event -> event.getRowValue().setType(event.getNewValue()));


        speciesColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        speciesColumn.setOnEditCommit(event -> event.getRowValue().setSpecies(event.getNewValue()));

    }

    @FXML
    private void help(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(rb.getString("aboutTitle"));
        alert.setHeaderText(rb.getString("aboutHeader"));
        alert.setContentText(rb.getString("aboutContent"));
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait();
    }

    private static List<Ent> prepare() {
        Ent ent1 = new Ent(1922, 550, "Brzoza", Ent.Type.LEAFED);
        Ent ent2 = new Ent(2012, 75, "Sosna", Ent.Type.CONIFEROUS);
        Ent ent3 = new Ent(1835, 350, "Sosna", Ent.Type.CONIFEROUS);
        Ent ent4 = new Ent(2017, 23, "Sosna", Ent.Type.CONIFEROUS);

        List<Ent> listOfEnts = new ArrayList<>();
        listOfEnts.add(ent1);
        listOfEnts.add(ent2);
        listOfEnts.add(ent3);
        listOfEnts.add(ent4);
        return listOfEnts;
    }

    @FXML
    private void saveFile(ActionEvent actionEvent) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(null);
        if (file != null) {
            save(file);
        }
    }

    private void save(File file) {
        SaveWorker worker = new SaveWorker(entsList, file);
        progressBar.progressProperty().bind(worker.progressProperty());
        new Thread(worker).start();
    }

    @FXML
    public void openFile(ActionEvent actionEvent) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(null);
        if (file != null){
            open(file);
        }
    }

    private void open(File file) {
        OpenWorker openWorker = new OpenWorker(entsList, file);
        progressBar.progressProperty().bind(openWorker.progressProperty());
        new Thread(openWorker).start();
    }

    @FXML
    public void plant(ActionEvent actionEvent) {
        entsList.add(new Ent(2018, 0, "New species", Ent.Type.UNDEFINED));
    }

    @FXML
    public void remove(ActionEvent actionEvent) {
        if (entsView.getSelectionModel().getSelectedIndex() >= 0) {
            entsList.remove(entsView.getSelectionModel().getSelectedIndex());
            entsView.getSelectionModel().clearSelection();
        }
    }

    public void rootSeedling(ActionEvent actionEvent) {
        if (entsView.getSelectionModel().getSelectedIndex() >= 0) {
            Ent selectedEnt = entsView.getSelectionModel().getSelectedItem();
            entsList.add(new Ent(2018, 10, selectedEnt.getSpecies(), selectedEnt.getType()));
            entsView.getSelectionModel().clearSelection();
        }
    }
}
